VU=src/main/java/com/yoncabt/ebr/util/VersionUtil.java
JDK=jdk-8u172-linux-x64.tar.gz
JDK_URL=http://download.yoncabt.com.tr/d/$(JDK)

war:
	./mvnw clean install -DskipTests

JDK_NAME=yonca-oracle-jdk8

deb:
	test -d reports.orig || git clone git@gitlab.com:YoncaBilisim/abys-reports.git reports.orig
	cd reports.orig && git pull && git fsck && git gc
	$(eval VERSION_ID := $(shell git log | wc -l))
	chmod 600 .ssh/*
	cp --force -v target/ebr.war .
	fpm -a all \
	--input-type dir \
	--output-type deb \
	--name ebr \
	--version $(VERSION_ID) \
	--url http://gitlab.com/YoncaBilisim/ebr \
	--after-install fpm/after-install.sh \
	--before-install fpm/before-install.sh \
	--deb-no-default-config-files \
	--description "eastblue reporting from yonca bilisim" \
	--vendor YoncaBilisim \
	--depends git \
	--depends $(JDK_NAME) \
	--depends ttf-mscorefonts-installer --depends util-linux --depends sudo \
	ebr.war=/var/ebr/ \
	ebr.conf.orig=/var/ebr/ \
	reports.orig=/var/ebr/ \
	logo.png.orig=/var/ebr/ \
	.ssh=/var/ebr/ \
	keystore=/var/ebr/ \
	99-ebr-sudo=/etc/sudoers.d/

publish-deb:
	cd ~/repo && make pull
	rm -f ~/repo/ebr-*.war
	cp target/ebr.war ~/repo/ebr-`git log | wc -l`.war
	reprepro -b ~/repo/ includedeb debian ebr_*.deb
	# silme kodu
	# reprepro -Vb ~/repo list debian
	# reprepro -Vb ~/repo remove debian abys
	cd ~/repo && make publish
	mv ebr_*.deb target


check-deb:
	mkdir -p tmp && cd tmp && ar x ../ebr_*_amd64.deb && tar tvf data.tar.gz | less
	mkdir -p tmp && cd tmp && ar x ../ebr_*_amd64.deb && tar tvf control.tar.gz | less

clean:
	rm -rfv target *.deb *.war

builder:
	git push origin master
	ssh yonca-builder "cd ebr/ebr && git pull && make build-on-builder"

build-on-builder:
	git pull origin master
	PATH="$$HOME/jdk8/bin:$$PATH" ./mvnw clean install -DskipTests
	cd target && md5sum ebr.war > md5sum.txt && mv ebr.war ~/builder.yoncabt.com.tr/ebr && mv md5sum.txt ~/builder.yoncabt.com.tr/ebr

BUILD_DIR=/tmp/build
TARGET_TMP=$(BUILD_DIR)/apps
JDK_URL=http://download.yoncabt.com.tr/d/jdk-8u172-linux-x64.tar.gz

build-on-mailbuilder:
	make clean
	mkdir -p $(TARGET_TMP)
	wget --quiet --continue -O $(TARGET_TMP)/_jdk.tar.gz $(JDK_URL)
	cd $(TARGET_TMP) && tar xf _jdk.tar.gz && mv $(TARGET_TMP)/jdk* $(TARGET_TMP)/jdk
	JAVA_HOME=$(TARGET_TMP)/jdk make versiyon
	JAVA_HOME=$(TARGET_TMP)/jdk JDK_HOME=$(TARGET_TMP)/jdk make war
	cd $(BUILD_DIR) && make deb
	cd ~/repo && make pull
	make publish-deb

versiyon:
	sed -i "0,/RE/s/WAR_DATE = [^;]*/WAR_DATE = 1000L * `date +%s`/" $(VU)
	sed -i "0,/RE/s/GIT_ID = [^;]*/GIT_ID = \"`git rev-parse HEAD`\"/" $(VU)
	git commit $(VU) -m "versiyon dosyası değişikliği"

release: clean versiyon war deb publish-deb

