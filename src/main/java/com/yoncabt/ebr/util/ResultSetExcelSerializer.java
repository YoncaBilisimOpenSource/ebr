package com.yoncabt.ebr.util;

import com.yoncabt.ebr.ui.FieldType;
import org.apache.commons.io.output.FileWriterWithEncoding;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 * @author myururdurmaz
 */
public class ResultSetExcelSerializer {

    private ResultSet res;
    private File file;
    private List<String> columnNames = new ArrayList<>();

    private List<FieldType> types = new ArrayList<>();

    public ResultSetExcelSerializer(ResultSet res, File file) {
        this.res = res;
        this.file = file;
    }

    public File serialize() throws SQLException, IOException {
        try (FileWriterWithEncoding fw = new FileWriterWithEncoding(file, "utf-8")) {
            HSSFWorkbook hssfWorkBook = new HSSFWorkbook();
            generateWorksheet("Rapor", hssfWorkBook, res);
            FileOutputStream fileOut = new FileOutputStream(file);
            hssfWorkBook.write(fileOut);
        }
        return file;
    }

    private static void generateHeaderRow(HSSFSheet sheet, ResultSet rs) throws SQLException {

        HSSFRow headerRow = sheet.createRow(0);

        ResultSetMetaData resultsetMetadata = rs.getMetaData();
        int columnCount = resultsetMetadata.getColumnCount();

        for (int i = 0; i < columnCount; i++) {
            HSSFCell cell = headerRow.createCell(i);
            cell.setCellValue(resultsetMetadata.getColumnName(i + 1));
        }
    }


    private static void populateRows(HSSFSheet sheet, ResultSet rs) throws SQLException {
        int rowCounter = 1;
        while (rs.next()) {

            HSSFRow row = sheet.createRow(rowCounter);
            int columnCount = rs.getMetaData().getColumnCount();

            for (int i = 0; i < columnCount; i++) {
                if (rs.getString(i + 1) == null)
                    continue;
                switch (rs.getMetaData().getColumnType(i + 1)) {
                    case Types.VARCHAR:
                    case Types.CHAR:
                    case Types.NCHAR:
                    case Types.NVARCHAR:
                        row.createCell(i).setCellValue(rs.getString(i + 1));
                        break;
                    case Types.DATE:
                    case Types.TIMESTAMP:
                    case Types.TIMESTAMP_WITH_TIMEZONE:
                        row.createCell(i).setCellValue(rs.getDate(i + 1));
                        break;
                    default:
                        row.createCell(i).setCellValue(new BigDecimal(rs.getDouble(i + 1)).doubleValue());
                        break;
                }

            }
            rowCounter++;
        }
    }

    /**
     * Generate a worksheet and populate it with a header row and data rows
     *
     * @param workSheetName name of the worksheet
     * @param workbook      a reference to the HFFSWorkbook (Apache POI)
     * @param resultSet     a JDBC resultset containing the data
     * @throws SQLException if any error occurs
     */
    private static void generateWorksheet(String workSheetName, HSSFWorkbook workbook, ResultSet resultSet) throws SQLException {
        HSSFSheet workSheet = workbook.createSheet(workSheetName);

        // Create the first Header row
        // Get all the column names from the ResultSet
        generateHeaderRow(workSheet, resultSet);

        // Populate the data in the rows
        populateRows(workSheet, resultSet);
    }
}
