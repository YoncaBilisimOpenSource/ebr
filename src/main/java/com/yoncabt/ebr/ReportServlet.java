/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoncabt.ebr;

import com.yoncabt.ebr.executor.ReportTask;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author myururdurmaz
 */
@WebServlet(name = "ReportServlet", urlPatterns = {"/ReportServlet"})
public class ReportServlet extends HttpServlet {

    @Autowired
    private ReportService reportService;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String mesaj = "Durum bilinmiyor ???";
        String nextAction = "";
        boolean refresh = false;
        String reportId = request.getParameter("reportId");
        boolean html = false;
        try {
            request.setCharacterEncoding("utf-8");
            String action = request.getParameter("action");
            if ("download".equals(action)) {
                ReportTask detail = reportService.detail(reportId);
                String mimeType = StringUtils.defaultIfBlank(request.getParameter("content-type"), ReportOutputFormat.valueOf(detail.getRequest().getExtension().toLowerCase()).getMimeType());
                response.setContentType(mimeType);
                String ext = "";
                String fileName = reportId;
                if (StringUtils.isNotBlank(request.getParameter("ext")))
                    ext = "." + request.getParameter("ext");
                if (StringUtils.isNotBlank(request.getParameter("fileName")))
                    fileName = request.getParameter("fileName");
                response.setHeader("Content-Disposition", "inline; filename=\"" + fileName + ext + "\"");
                InputStream is = reportService.output(reportId);
                IOUtils.copy(is, response.getOutputStream());
                response.getOutputStream().close();
                return;
            } else if ("special-download".equals(action)) {
                ReportTask detail = reportService.detail(reportId);
                String mimeType = ReportOutputFormat.valueOf(detail.getRequest().getExtension().toLowerCase()).getMimeType();
                response.setContentType(mimeType);
                InputStream is = reportService.output(reportId);
                IOUtils.copy(is, response.getOutputStream());
                response.getOutputStream().close();
                return;
            } else if ("session-download".equals(action)) {
                response.setContentType(request.getParameter("content-type"));
                if (!StringUtils.isBlank(request.getParameter("file-name"))) {
                    response.setHeader("Content-Disposition", "inline; filename=\"" + request.getParameter("file-name") + "\"");
                }
                String reportString = (String) request.getSession(true).getAttribute(reportId);
                byte[] rep = reportString.getBytes("utf-8");
                InputStream is = new ByteArrayInputStream(rep);
                IOUtils.copy(is, response.getOutputStream());
                response.getOutputStream().close();
                return;
            } else if ("pdf-download".equals(action)) {           // Otomatik Yonca-Printer a Pdf Göndermek için kullanılacak koşul. kurtcu.tamer - 10.09.2019
                response.setContentType(request.getParameter("content-type"));
                if (!StringUtils.isBlank(request.getParameter("file-name"))) {
                    response.setHeader("Content-Disposition", "inline; filename=\"" + request.getParameter("file-name") + "\"");
                }
                byte[] rep = (byte[]) request.getSession(true).getAttribute(reportId);
                InputStream is = new ByteArrayInputStream(rep);
                IOUtils.copy(is, response.getOutputStream());
                response.getOutputStream().close();
                return;
            } else if ("status".equals(action)) {
                html = true;
                ReportTask detail = reportService.detail(reportId);
                if (detail == null) {//başlamamış
                    mesaj = "Böyle bir rapor tanımıyorum";
                    nextAction = "";
                    refresh = false;
                } else {
                    switch (detail.getStatus()) {
                        case WAIT:
                            mesaj = "Daha başlamamış";
                            nextAction = "status";
                            refresh = true;
                            break;

                        case RUN:
                            mesaj = "Çalışıyor ...";
                            nextAction = "status";
                            refresh = true;
                            break;

                        case EXCEPTION:
                            mesaj = "Hata oluştu " + detail.getException();
                            nextAction = "";
                            refresh = false;
                            break;

                        case FINISH:
                            mesaj = "Bitmiş";
                            nextAction = "download";
                            refresh = true;
                            break;

                        case CANCEL:
                            mesaj = "İptal edilmiş";
                            nextAction = "";
                            refresh = false;
                            break;

                        case SCHEDULED:
                            mesaj = "Daha başlamamış";
                            nextAction = "status";
                            refresh = true;
                            break;
                        default:
                            throw new IllegalArgumentException(detail.getStatus().name());
                    }
                }
            } else {
                throw new IllegalArgumentException(action);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            mesaj = "Hata oldu. " + ex.getMessage();
            nextAction = "";
            refresh = false;
            html = true;
        } finally {
            if (html) {
                response.setHeader("Content-Type", "text/html");
                PrintWriter out = response.getWriter();
                out.write("<!doctype html>");
                out.write("<html><head>");
                out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");
                if (refresh)
                    out.write("<meta http-equiv=\"refresh\" content=\"" + 3 + "; url=/ebr/ReportServlet?action=" + nextAction + "&reportId=" + reportId + "\">");
                out.write("<title>Raporlanıyor ....</title>");
                out.write("</head><body>");
                out.write("<h1>" + mesaj + "</h1>");
                out.write("</body></html>");
                out.close();
            }
        }
    }

    private String trDecode(String s) {
        return s.replace("~U", "Ü")
                .replace("~G", "Ğ")
                .replace("~I", "İ")
                .replace("~S", "Ş")
                .replace("~C", "Ç")
                .replace("~O", "Ö")
                .replace("~u", "ü")
                .replace("~g", "ğ")
                .replace("~i", "ı")
                .replace("~s", "ş")
                .replace("~c", "ç")
                .replace("~o", "ö");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "hamili kart yakinimdir ;)";
    }// </editor-fold>
}
