/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoncabt.ebr.logger.db;

import com.yoncabt.abys.core.util.EBRConf;
import com.yoncabt.abys.core.util.EBRParams;
import com.yoncabt.ebr.ReportRequest;
import com.yoncabt.ebr.executor.jasper.Convert;
import com.yoncabt.ebr.jdbcbridge.pool.DataSourceManager;
import com.yoncabt.ebr.jdbcbridge.pool.EBRConnection;
import java.io.File;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author myururdurmaz
 */
@Component
public class DBReportUserLogger {

    @Autowired
    private DataSourceManager dataSourceManager;

    @PostConstruct
    private void init() {
        System.out.println("EBR report.logger.actionLogger.active:" + EBRConf.INSTANCE.getValue("report.logger.actionLogger.active", false));
    }

    @PreDestroy
    private void destroy() throws SQLException {
    }

    public void logReport(ReportRequest request) throws SQLException {
        boolean active = EBRConf.INSTANCE.getValue("report.logger.actionLogger.active", false);
        if (!active)
            return;

        try (EBRConnection con = dataSourceManager.get("default", request.getUser(), "EBR", getClass().getSimpleName());){
            con.setAutoCommit(false);

            String sql = String.format("INSERT INTO IT_ACTION_LOG (id, CORPORATION_ID, USER_ID, ACTION_DATE, IP, APPLICATION, \"METHOD\", DESCRIPTION, CREATED_USER_ID, CREATED_TIMESTAMP, VERSION)\n" +
                                "SELECT IT_ACTION_LOG_SEQ.nextval AS id, DEFAULT_CORPORATION_ID, id AS user_id, sysdate AS action_date, '?' AS ip, 'EBR' AS application, ? AS METHOD, 'N/A' AS description, id AS CREATED_USER_ID, SYSTIMESTAMP AS CREATED_TIMESTAMP, 0 AS VERSION FROM IT_USER WHERE ID = ?");
            try (PreparedStatement ps = con.prepareStatement(sql)) {
                final String reportPath = (String)request.getReportParams().get("REPORT_PATH");
                String reportBasePath = EBRConf.INSTANCE.getValue(EBRParams.REPORTS_JRXML_PATH, "/home/myururdurmaz/reports");
                File reportFile = new File(reportBasePath, reportPath);
                File folderConfig = new File(reportFile.getParentFile(), EBRParams.FOLDER_EBR_JSON);
                String title = reportPath;

                if (folderConfig.exists()) {
                    try {
                        JSONObject jo = new JSONObject(FileUtils.readFileToString(folderConfig, "utf-8"));
                        title = jo.optString("label", reportPath);
                    } catch (Exception ex) {
                        throw new SQLException(folderConfig.getAbsolutePath(), ex);
                    }
                }


                ps.setString(1, title);
                ps.setLong(2, Convert.to(request.getReportParams().get("_USER_ID"), Long.class));

                ps.executeUpdate();
                con.commit();
            }
        }
    }

}
