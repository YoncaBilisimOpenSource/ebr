/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoncabt.ebr.security.exceptions;

import com.yoncabt.ebr.exceptions.ReportException;

/**
 *
 * @author myururdurmaz
 */
public class SecurityException extends ReportException {

    public SecurityException() {
        super(null, null);
    }

    public SecurityException(String message) {
        super(null, null, message);
    }

    public SecurityException(Throwable cause) {
        super(null, null, cause);
    }

    public SecurityException(String message, Throwable cause) {
        super(null, null, message, cause);
    }
}
