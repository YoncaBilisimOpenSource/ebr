package com.yoncabt.ebr.exceptions;

import com.yoncabt.ebr.ReportRequest;
import java.io.File;

/**
 *
 * @author myururdurmaz
 */
public class ReportException extends RuntimeException {
    
    private ReportRequest request;
    private File jrxml;

    public ReportException(File jrxml, ReportRequest request) {
        this.jrxml = jrxml;
        this.request = request;
    }

    public ReportException(File jrxml, ReportRequest request, String message) {
        super(message);
        this.jrxml = jrxml;
        this.request = request;
    }

    public ReportException(File jrxml, ReportRequest request, Throwable cause) {
        super(cause);
        this.jrxml = jrxml;
        this.request = request;
    }

    public ReportException(File jrxml, ReportRequest request, String message, Throwable cause) {
        super(message, cause);
        this.request = request;
    }

    public ReportRequest getRequest() {
        return request;
    }

    public void setRequest(ReportRequest request) {
        this.request = request;
    }

    public File getJrxml() {
        return jrxml;
    }
}
