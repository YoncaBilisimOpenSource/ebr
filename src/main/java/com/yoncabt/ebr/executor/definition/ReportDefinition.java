/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoncabt.ebr.executor.definition;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author myururdurmaz
 */
public class ReportDefinition {

    private String caption;
    private List<ReportParam> reportParams = new ArrayList<>();
    private ReportType reportType;
    private String dataSource;
    private String textEncoding;
    private String textTemplate;
    private File file;
    private String filePath;
    private Double charWidth;
    private Double charHeight;
    private Integer pageWidthInChars;
    private Integer pageHeightInChars;
    private String pageSeparator;
    private String lineSeparator;

    public ReportDefinition(File file) {
        this.file = file;
    }

    public List<ReportParam> getReportParams() {
        return reportParams;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    /**
     * @param reportParams the reportParams to set
     */
    public void setReportParams(List<ReportParam> reportParams) {
        this.reportParams = reportParams;
    }

    /**
     * @return the reportType
     */
    public ReportType getReportType() {
        return reportType;
    }

    /**
     * @param reportType the reportType to set
     */
    public void setReportType(ReportType reportType) {
        this.reportType = reportType;
    }

    /**
     * @return the dataSource
     */
    public String getDataSource() {
        return dataSource;
    }

    /**
     * @param dataSource the dataSource to set
     */
    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * @return the textEncoding
     */
    public String getTextEncoding() {
        return textEncoding;
    }

    /**
     * @param textEncoding the textEncoding to set
     */
    public void setTextEncoding(String textEncoding) {
        this.textEncoding = textEncoding;
    }

    /**
     * @return the file
     */
    public File getFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(File file) {
        this.file = file;
    }

    /**
     * @return the textTemplate
     */
    public String getTextTemplate() {
        return textTemplate;
    }

    /**
     * @param textTemplate the textTemplate to set
     */
    public void setTextTemplate(String textTemplate) {
        this.textTemplate = textTemplate;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * @return the charWidth
     */
    public Double getCharWidth() {
        return charWidth;
    }

    /**
     * @param charWidth the charWidth to set
     */
    public void setCharWidth(Double charWidth) {
        this.charWidth = charWidth;
    }

    /**
     * @return the charHeight
     */
    public Double getCharHeight() {
        return charHeight;
    }

    /**
     * @param charHeight the charHeight to set
     */
    public void setCharHeight(Double charHeight) {
        this.charHeight = charHeight;
    }

    /**
     * @return the pageWidthInChars
     */
    public Integer getPageWidthInChars() {
        return pageWidthInChars;
    }

    /**
     * @param pageWidthInChars the pageWidthInChars to set
     */
    public void setPageWidthInChars(Integer pageWidthInChars) {
        this.pageWidthInChars = pageWidthInChars;
    }

    /**
     * @return the pageHeightInChars
     */
    public Integer getPageHeightInChars() {
        return pageHeightInChars;
    }

    /**
     * @param pageHeightInChars the pageHeightInChars to set
     */
    public void setPageHeightInChars(Integer pageHeightInChars) {
        this.pageHeightInChars = pageHeightInChars;
    }

    /**
     * @return the pageSeparator
     */
    public String getPageSeparator() {
        return pageSeparator;
    }

    /**
     * @param pageSeparator the pageSeparator to set
     */
    public void setPageSeparator(String pageSeparator) {
        this.pageSeparator = pageSeparator;
    }

    /**
     * @return the lineSeparator
     */
    public String getLineSeparator() {
        return lineSeparator;
    }

    /**
     * @param lineSeparator the lineSeparator to set
     */
    public void setLineSeparator(String lineSeparator) {
        this.lineSeparator = lineSeparator;
    }
}
