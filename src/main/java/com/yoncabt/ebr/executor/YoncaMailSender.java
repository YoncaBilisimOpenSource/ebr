/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoncabt.ebr.executor;

import com.yoncabt.abys.core.util.EBRConf;
import java.io.File;
import java.util.Map;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

/**
 *
 * @author myururdurmaz
 */
@Component
@Scope(scopeName = "prototype")
public class YoncaMailSender {

    @Autowired
    private JavaMailSender mailSender;

    /*
        spring.mail.host=smtp.gmail.com
	spring.mail.port=587
	spring.mail.username=<login user to smtp server>
	spring.mail.password=<login password to smtp server>
	spring.mail.properties.mail.smtp.auth=true
	spring.mail.properties.mail.smtp.starttls.enable=true
    */

    public boolean isConfigured() {
        return EBRConf.INSTANCE.hasValue("report.smtp.server.address")
                && EBRConf.INSTANCE.hasValue("report.smtp.server.address")
                && EBRConf.INSTANCE.hasValue("report.smtp.username")
                && EBRConf.INSTANCE.hasValue("report.smtp.password");
    }

    public void send(String to, String subject, String text, Map<String, File> attachments) throws MessagingException {

        MimeMessage mm = mailSender.createMimeMessage();
        MimeMessageHelper mmh = new MimeMessageHelper(mm, true);
        mmh.setFrom(EBRConf.INSTANCE.getValue("report.smtp.username", ""));
        mmh.setTo(to);
        mmh.setText(text);
        mmh.setSubject(text);
        for (Map.Entry<String, File> entrySet : attachments.entrySet()) {
            String key = entrySet.getKey();
            final File value = entrySet.getValue();
            mmh.addAttachment(key, value);

        }
        mailSender.send(mm);
    }

}
