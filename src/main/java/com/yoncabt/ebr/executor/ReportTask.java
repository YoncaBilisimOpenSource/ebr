/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoncabt.ebr.executor;

import com.yoncabt.abys.core.util.log.FLogManager;
import com.yoncabt.ebr.ReportOutputFormat;
import com.yoncabt.ebr.ReportRequest;
import com.yoncabt.ebr.ReportResponse;
import com.yoncabt.ebr.exceptions.ReportException;
import com.yoncabt.ebr.executor.definition.ReportDefinition;
import com.yoncabt.ebr.executor.jasper.JasperReport;
import com.yoncabt.ebr.executor.sql.SQLReport;
import com.yoncabt.ebr.jdbcbridge.pool.DataSourceManager;
import com.yoncabt.ebr.jdbcbridge.pool.EBRConnection;
import com.yoncabt.ebr.logger.ReportLogger;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.InputStream;
import java.util.Collections;
import org.apache.commons.lang.time.DurationFormatUtils;

/**
 * @author myururdurmaz
 */
@Component
@Scope("prototype")
public class ReportTask implements Runnable, Comparable<ReportTask> {

    private volatile Status status = Status.WAIT;

    @Autowired
    private JasperReport jasperReports;

    @Autowired
    private YoncaMailSender mailSender;

    @Autowired
    private DataSourceManager dataSourceManager;

    @Autowired
    private ReportLogger reportLogger;

    @Autowired
    private ApplicationContext context;

    private static FLogManager logManager = FLogManager.getLogger(ReportTask.class);

    private ReportRequest request;

    private ReportResponse response;

    private ReportException exception;

    private EBRConnection connection;

    private long started;

    private long ended;

    private boolean sentToClient;

    @Override
    public void run() {
        if (status == Status.CANCEL) {
            return;
        }
        status = Status.RUN;
        logManager.info(request.getUuid() + " başladı");
        synchronized (this) {
            started = System.currentTimeMillis();
        }
        if (response == null) {
            response = new ReportResponse();
            response.setUuid(request.getUuid());
        }
        Thread.currentThread().setName(request.getReport() + " " + request.getUuid());
        try {
            //FIXME support for sql
            BaseReport report;
            ReportDefinition definition;
            if (request.getReport().endsWith(".jrxml")) {
                report = new JasperReport();
                definition = report.loadDefinition(JasperReport.getReportFile(request.getReport()));
            } else if (request.getReport().endsWith(".sql")) {
                report = new SQLReport();
                definition = ((SQLReport) report).loadDefinition(JasperReport.getReportFile(request.getReport()));
            } else {
                throw new IllegalArgumentException(request.getReport());
            }
            if (StringUtils.isEmpty(request.getDatasourceName())) {
                request.setDatasourceName(definition.getDataSource());
            }
            if (StringUtils.isEmpty(request.getDatasourceName())) {
                request.setDatasourceName("default");
            }
            context.getAutowireCapableBeanFactory().autowireBean(report);
            connection = dataSourceManager.get(request.getDatasourceName(), StringUtils.defaultIfBlank(request.getClientInfo(), "DUMMY"), "EBR", request.getReport());
            if (request.getReportQuery() == null && request.getReport() != null) {
                request.setReportQuery(FileUtils.readFileToString(new File(request.getReport())));
            }
            report.exportTo(request, ReportOutputFormat.valueOf(request.getExtension().toLowerCase()), connection, definition);
            try (InputStream is = reportLogger.getReportData(request.getUuid())) {
                File tmpFile = File.createTempFile("ebr_mail", ".tmp");
                FileUtils.copyInputStreamToFile(is, tmpFile);
                if (!StringUtils.isBlank(request.getEmail()) && mailSender.isConfigured()) {
                    logManager.info("mail gönderilecek :" + request.getEmail());
                    mailSender.send(request.getEmail(), definition.getCaption(), "Raporunuz ektedir", Collections.singletonMap(request.getUuid() + "." + request.getExtension(), tmpFile));
                } else {
                    logManager.info("mail pasif durumda");
                }
                tmpFile.delete();
            }
            logManager.info(request.getUuid() + " bitti");
            status = Status.FINISH;
        } catch (Exception ex) {
            status = Status.EXCEPTION;
            synchronized (this) {
                if (ex instanceof ReportException) {
                    exception = (ReportException) ex;
                    if (exception.getRequest() == null) {
                        exception.setRequest(request);
                    }
                } else {
                    exception = new ReportException(null, request, ex);
                }
            }
            logManager.error(request.getUuid() + " hata", exception);
        } finally {
            if (connection != null) {
                connection.close();
            }
            connection = null;//çöp kalmasın
        }
        synchronized (this) {
            ended = System.currentTimeMillis();
        }
        long timeDiff = System.currentTimeMillis() - getStarted();
        if (timeDiff > 60_000) {
            logManager.error(request.getUuid() + " başlayalı " + DurationFormatUtils.formatDurationHMS(timeDiff) + " oldu ancak bitti", null);
        }
    }

    public ReportRequest getRequest() {
        return request;
    }

    public void setRequest(ReportRequest request) {
        this.request = request;
    }

    /**
     * @return the exception
     */
    public ReportException getException() {
        return exception;
    }

    public ReportResponse getResponse() {
        return response;
    }

    public void setResponse(ReportResponse response) {
        this.response = response;
    }

    public EBRConnection getYoncaConnection() {
        return connection;
    }

    public long getStarted() {
        return started;
    }

    public long getEnded() {
        return ended;
    }

    public void cancel() {
        if (status != Status.FINISH) {
            status = Status.CANCEL;
        }
        if (connection != null) {
            connection.cancel();
            connection = null;//çöp kalmasın
        }
    }

    /**
     * @return the status
     */
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public int compareTo(ReportTask o) {
        return Long.valueOf(this.getStarted()).compareTo(o.getStarted());
    }

    /**
     * @return the sentToClient
     */
    public boolean isSentToClient() {
        return sentToClient;
    }

    /**
     * @param sentToClient the sentToClient to set
     */
    public void setSentToClient(boolean sentToClient) {
        this.sentToClient = sentToClient;
    }
}
