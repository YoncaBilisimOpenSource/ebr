/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoncabt.ebr.executor.jasper;

import com.yoncabt.abys.core.util.EBRConf;
import com.yoncabt.ebr.executor.definition.ReportDefinition;
import net.sf.jasperreports.export.SimpleTextReportConfiguration;

/**
 *
 * @author myururdurmaz
 */
public class YoncaTextReportConfiguration extends SimpleTextReportConfiguration {

    private String template;
    private ReportDefinition definition;

    public YoncaTextReportConfiguration(ReportDefinition definition, String template) {
        this.definition = definition;
        this.template = template;
    }

    @Override
    public Float getCharWidth() {
        if (definition != null && definition.getCharWidth() != null && definition.getCharWidth() != 0)
            return definition.getCharWidth().floatValue();
        return EBRConf.INSTANCE.getValue("report.texttemplate." + template + ".CHAR_WITDH", super.getCharWidth());
    }

    @Override
    public Float getCharHeight() {
        if (definition != null && definition.getCharHeight() != null && definition.getCharHeight() != 0)
            return definition.getCharHeight().floatValue();
        return EBRConf.INSTANCE.getValue("report.texttemplate." + template + ".CHAR_HEIGHT", super.getCharHeight());
    }

    @Override
    public Integer getPageWidthInChars() {
        if (definition != null && definition.getPageWidthInChars() != null && definition.getPageWidthInChars() != 0)
            return definition.getPageWidthInChars();
        return EBRConf.INSTANCE.getValue("report.texttemplate." + template + ".PAGE_WIDTH_CHARS", super.getPageWidthInChars());
    }

    @Override
    public Integer getPageHeightInChars() {
        if (definition != null && definition.getPageHeightInChars() != null && definition.getPageHeightInChars() != 0)
            return definition.getPageHeightInChars();
        return EBRConf.INSTANCE.getValue("report.texttemplate." + template + ".PAGE_HEIGHT_CHARS", super.getPageHeightInChars());
    }

}
