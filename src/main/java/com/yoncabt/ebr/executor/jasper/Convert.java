/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoncabt.ebr.executor.jasper;

import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.util.ResourceBundle;

/**
 * @author myururdurmaz
 */
public class Convert {

    /**
     * burası jrxml e gidecek verilerin tip dönüşümünü yapar.
     *
     * @param <T>
     * @param val
     * @param valueClass
     * @return
     */
    public static <T> T to(Object val, Class<T> valueClass) {
        try {
            return toInner(val, valueClass);
        } catch (Exception e) {
            throw new IllegalArgumentException(val + "-> " + valueClass.getName() + " dönümüşü yapılırken hata", e);
        }
    }
    private static <T> T toInner(Object val, Class<T> valueClass) {
        if (val == null) {
            return null;
        }
        if (valueClass.isInstance(val)) {
            return (T) val;
        }
        if (ResourceBundle.class == valueClass) { //bunu dışarı çıkartayım
            return (T) val;
        }
        if (val instanceof String) {
            if (Number.class.isAssignableFrom(valueClass))
                return str2Number((String) val, valueClass);
        }
        if (val instanceof Number && Number.class.isAssignableFrom(valueClass)) {
            Number n = (Number) val;
            if (valueClass == Long.class || valueClass == Long.TYPE) {
                return (T) new Long(n.longValue());
            }
            if (valueClass == Integer.class || valueClass == Integer.TYPE) {
                return (T) new Integer(n.intValue());
            }
            if (valueClass == Double.class || valueClass == Double.TYPE) {
                return (T) new Double(n.doubleValue());
            }
            if (valueClass == BigDecimal.class) {
                return (T) new BigDecimal(n.toString());
            }
        }

        throw new IllegalArgumentException(val.getClass() + "->" + valueClass.getName());

    }

    private static <T> T str2Number(String val, Class<T> valueClass) {
        if (StringUtils.isBlank(val)) {
            return null;
        }
        if (valueClass == Integer.class) {
            return (T) Integer.valueOf(val);
        }
        if (valueClass == Long.class) {
            return (T) Long.valueOf(val);
        }
        if (valueClass == Short.class) {
            return (T) Short.valueOf(val);
        }
        if (valueClass == Double.class) {
            return (T) Double.valueOf(val);
        }
        if (valueClass == BigDecimal.class) {
            return (T) new BigDecimal(val);
        }
        throw new IllegalArgumentException(val.getClass() + "->" + valueClass.getName());
    }

}
