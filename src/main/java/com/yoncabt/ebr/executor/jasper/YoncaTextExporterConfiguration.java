/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoncabt.ebr.executor.jasper;

import com.yoncabt.abys.core.util.EBRConf;
import com.yoncabt.ebr.executor.definition.ReportDefinition;
import net.sf.jasperreports.export.TextExporterConfiguration;
import org.apache.commons.lang.StringEscapeUtils;

/**
 *
 * @author myururdurmaz
 */
public class YoncaTextExporterConfiguration implements TextExporterConfiguration {

    private String template;
    private ReportDefinition definition;

    public YoncaTextExporterConfiguration(ReportDefinition definition, String template) {
        this.definition = definition;
        this.template = template;
    }

    @Override
    public String getPageSeparator() {
        if (definition != null && definition.getPageSeparator() != null)
            return definition.getPageSeparator();
        return StringEscapeUtils.unescapeJava(EBRConf.INSTANCE.getValue("report.texttemplate." + template + ".BETWEEN_PAGES_TEXT", "\n\n"));
    }

    @Override
    public String getLineSeparator() {
        if (definition != null && definition.getLineSeparator() != null)
            return definition.getLineSeparator();
        return StringEscapeUtils.unescapeJava(EBRConf.INSTANCE.getValue("report.texttemplate." + template + ".LINE_SEPARATOR", "\n"));
    }

    @Override
    public Boolean isTrimLineRight() {
        return true;
    }

    @Override
    public Boolean isOverrideHints() {
        return true;//emin dğeilim
    }

}
