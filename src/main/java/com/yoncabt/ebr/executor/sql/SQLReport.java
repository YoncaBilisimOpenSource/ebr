/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoncabt.ebr.executor.sql;

import com.yoncabt.ebr.ReportOutputFormat;
import com.yoncabt.ebr.ReportRequest;
import com.yoncabt.ebr.exceptions.ReportException;
import com.yoncabt.ebr.executor.BaseReport;
import com.yoncabt.ebr.executor.definition.ReportDefinition;
import com.yoncabt.ebr.executor.jasper.Convert;
import com.yoncabt.ebr.jdbcbridge.JDBCNamedParameters;
import com.yoncabt.ebr.jdbcbridge.pool.EBRConnection;
import com.yoncabt.ebr.logger.ReportLogger;
import com.yoncabt.ebr.ui.FieldType;
import com.yoncabt.ebr.util.ResultSetExcelSerializer;
import com.yoncabt.ebr.util.ResultSetSerializer;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;

/**
 * @author myururdurmaz
 */
@Component
@Scope(value = "request")
public class SQLReport extends BaseReport {

    @Autowired
    private ReportLogger reportLogger;

    @Override
    public ReportDefinition loadDefinition(File file) throws IOException {
        setFile(file);
        String jsonFilePath = StringUtils.removeEnd(file.getAbsolutePath(), ".sql") + ".ebr.json";
        File jsonFile = new File(jsonFilePath);
        return super.loadDefinition(file, jsonFile);
    }

    @Override
    public void exportTo(ReportRequest request, ReportOutputFormat outputFormat, EBRConnection con, ReportDefinition reportDefinition) throws ReportException, IOException {
        JDBCNamedParameters p = new JDBCNamedParameters(request.getReportQuery());
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        // ornek kullanım: "2001-07-04T12:08:56.235-0700"
        for (String param : request.getReportParams().keySet()) {
            LinkedHashMap<String, Object> mapParam = (LinkedHashMap) request.getReportParams().get(param);
            Object value = mapParam.get("value");
            FieldType type = mapParam.get("fieldType") == null ? FieldType.LONG : FieldType.valueOf(mapParam.get("fieldType").toString());
            switch (type) {
                case STRING: {
                    String convertedVal = Convert.to(value, String.class);
                    p.set(param, convertedVal);
                    break;
                }
                case INTEGER: {
                    Integer convertedVal = Convert.to(value, Integer.class);
                    p.set(param, convertedVal);
                    break;
                }
                case LONG: {
                    Long convertedVal = Convert.to(value, Long.class);
                    p.set(param, convertedVal);
                    break;
                }
                case DOUBLE: {
                    Double convertedVal = Convert.to(value, Double.class);
                    p.set(param, convertedVal);
                    break;
                }
                case BIGDECIMAL: {
                    BigDecimal convertedVal = Convert.to(value, BigDecimal.class);
                    p.set(param, convertedVal);
                    break;
                }
                case DATE: {
                    try {
                        value = dateFormat.parse((String) value);
                    } catch (ParseException e) {
                        throw new AssertionError(param + " tipi istenilen tarih formatında değil : Değer:" + value + " Format:" + dateFormat.toPattern(), e);
                    }
                    p.set(param, (Date) value);
                    break;
                }
                default:
                    throw new AssertionError(param + " in tipi tanınmıyor :" + type);
            }
        }

        try (
                PreparedStatement st = p.prepare(con);
                ResultSet res = st.executeQuery()) {
            File tempFile = File.createTempFile(request.getUuid(), outputFormat.name());
            if (outputFormat == ReportOutputFormat.xlsx || outputFormat == ReportOutputFormat.xls) {
                ResultSetExcelSerializer ser = new ResultSetExcelSerializer(res, tempFile);
                ser.serialize();
            } else if (outputFormat == ReportOutputFormat.json) {
                ResultSetSerializer ser = new ResultSetSerializer(res, tempFile);
                ser.serialize();
            }

            try (FileInputStream fis = new FileInputStream(tempFile)) {
                reportLogger.logReport(request, outputFormat, fis);
            }
            tempFile.delete();
        } catch (SQLException ex) {
            throw new ReportException(null, request, ex);
        }
    }
}
