/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoncabt.ebr.ws;

import com.yoncabt.abys.core.util.log.FLogManager;
import com.yoncabt.ebr.ReportOutputFormat;
import com.yoncabt.ebr.ReportRequest;
import com.yoncabt.ebr.ReportResponse;
import com.yoncabt.ebr.ReportService;
import com.yoncabt.ebr.executor.ReportTask;
import com.yoncabt.ebr.executor.YoncaMailSender;
import com.yoncabt.ebr.executor.definition.ReportDefinition;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.mvc.method.annotation.AbstractJsonpResponseBodyAdvice;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.time.DurationFormatUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author myururdurmaz
 */
@RestController
@Scope(scopeName = "request")
public class ReportWSv1_0 {

    @ControllerAdvice
    static class JsonpAdvice extends AbstractJsonpResponseBodyAdvice {
        public JsonpAdvice() {
            super("callback");
        }
    }

    private String getIp() {
        StringBuilder sb = new StringBuilder();
        try {
            Enumeration e = NetworkInterface.getNetworkInterfaces();
            while(e.hasMoreElements()) {
                NetworkInterface n = (NetworkInterface) e.nextElement();
                Enumeration ee = n.getInetAddresses();
                while (ee.hasMoreElements()) {
                    InetAddress i = (InetAddress) ee.nextElement();
                    if (i.getHostAddress().equals("127.0.0.1"))
                        continue;
                    if (i.getHostAddress().contains(":"))
                        continue;
                    sb.append("[").append(i.getHostAddress()).append("]");
                }
            }
        } catch (SocketException ex) {
            ex.printStackTrace();
        }
        return sb.toString();
    }

    @Autowired
    private ReportService reportService;

    @Autowired
    private YoncaMailSender mailSender;

    private static final FLogManager logManager = FLogManager.getLogger(ReportTask.class);

    @RequestMapping(
            value = {"/ws/1.0/mailTest/{target}"},
            method = RequestMethod.GET,
            produces = "application/json")
    public ResponseEntity<Map<String, Object>> mailTest(
            @PathVariable("target") String target
    ) throws MessagingException {
        Map<String, Object> ret = new HashMap<>();
        ret.put("mail aktif", mailSender.isConfigured());
        if (mailSender.isConfigured()) {
            mailSender.send(target, "test maili", getIp() + "\nebr test maili", new HashMap<>());
        }
        return ResponseEntity.ok(ret);
    }

    @RequestMapping(
            value = {"/ws/1.0/selfTest"},
            method = RequestMethod.GET,
            produces = "application/json")
    public ResponseEntity<List<String>> selfTest() {
        return ResponseEntity.ok(new ArrayList<>(reportService.dataSourceNames()));
    }

    @RequestMapping(
            value = {"/ws/1.0/dataSourceNames"},
            method = RequestMethod.GET,
            produces = "application/json")
    public ResponseEntity<List<String>> dataSourceNames() {
        return ResponseEntity.ok(new ArrayList<>(reportService.dataSourceNames()));
    }

    @RequestMapping(
            value = {"/ws/1.0/reportList"},
            method = RequestMethod.GET,
            produces = "application/json")
    public ResponseEntity<List<ReportDefinition>> reportList() throws IOException, JRException {
        return ResponseEntity.ok(reportService.reportList());
    }

    @RequestMapping(
            value = {"/ws/1.0/status/{requestId}"},
            method = RequestMethod.GET,
            produces = "application/json")
    public ResponseEntity<ReportResponse> status(
            @PathVariable("requestId") String requestId
    ) {
        ReportTask detail = reportService.detail(requestId);
        if (detail == null) {//başlamamış
            logManager.info("status query :YOK !!! " + requestId);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        logManager.info("status query :" + requestId);
        switch (detail.getStatus()) {
            case WAIT:
                logManager.info(detail.getRequest().getReport() + " status query :" + requestId + " :başlamış");
                return ResponseEntity.status(HttpStatus.CREATED).body(null);

            case RUN:
                logManager.info(detail.getRequest().getReport() + " status query :" + requestId + " :devam ediyor");
                long timeDiff = System.currentTimeMillis() - detail.getStarted();
                if (timeDiff > 60_000) {
                    logManager.error(requestId + " başlayalı " + DurationFormatUtils.formatDurationHMS(timeDiff) + " oldu ve bitmedi", null);
                }
                return ResponseEntity.status(HttpStatus.PROCESSING).body(null);

            case EXCEPTION:
                logManager.info(detail.getRequest().getReport() + " status query :" + requestId + " :hata");
                return ResponseEntity.status(420).body(null);// 420 Method Failure

            case FINISH:
                logManager.info(detail.getRequest().getReport() + " status query :" + requestId + " :bitmiş");
                return ResponseEntity.status(HttpStatus.OK).body(null);

            case CANCEL:
                logManager.info(detail.getRequest().getReport() + " status query :" + requestId + " :iptal");
                return ResponseEntity.status(HttpStatus.GONE).body(null);

            case SCHEDULED:
                logManager.info(detail.getRequest().getReport() + " status query :" + requestId + " :başlamış");
                return ResponseEntity.status(HttpStatus.CREATED).body(null);
            default:
                throw new IllegalArgumentException(detail.getStatus().name());
        }
    }

    @RequestMapping(
            value = {"/ws/1.0/reports"},
            method = RequestMethod.GET,
            produces = "application/json")
    public ResponseEntity<List<String>> reports() {
        return ResponseEntity.ok(reportService.reports());
    }

    @RequestMapping(
            value = {"/ws/1.0/detail/{requestId}"},
            method = RequestMethod.GET,
            produces = "application/json")
    public ResponseEntity<ReportTask> detail(
            @PathVariable("requestId") String requestId
    ) {
        ReportTask task = reportService.detail(requestId);
        if (task == null) {//başlamamış
            logManager.info("output :YOK !!! " + requestId);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        return ResponseEntity.status(HttpStatus.OK).body(task);
    }

    @RequestMapping(
            value = {"/ws/1.0/cancel/{requestId}"},
            method = RequestMethod.GET,
            produces = "application/json")
    public ResponseEntity<ReportTask> cancel(
            @PathVariable("requestId") String requestId
    ) {
        try {
            reportService.cancel(requestId);
            return ResponseEntity.status(HttpStatus.OK).body(null);
        } catch (NoSuchElementException e) {
            logManager.info("rapor :YOK !!! " + requestId);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @RequestMapping(
            value = {"/ws/1.0/output/{requestId}"},
            method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> output(
            @PathVariable("requestId") String requestId
    ) throws IOException {
        try {
            InputStream output = reportService.output(requestId);
            ReportTask task = reportService.detail(requestId);
            logManager.info("output :" + requestId);
            return ResponseEntity.status(HttpStatus.OK)
                    .contentType(ReportOutputFormat.valueOf(task.getRequest().getExtension().toLowerCase()).getMediaType())
                    .lastModified(task.getEnded())
                    .header("Content-Disposition", "inline; filename=" + requestId + "." + task.getRequest().getExtension())
                    .body(new InputStreamResource(output));
        } catch (NoSuchElementException e) {
            logManager.info("output :YOK !!! " + requestId);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @RequestMapping(
            value = {"/ws/1.0/error/{requestId}"},
            method = RequestMethod.GET,
            produces = "text/plain")
    public ResponseEntity<String> error(
            @PathVariable("requestId") String requestId
    ) throws IOException {
        return ResponseEntity.status(HttpStatus.OK).body(reportService.error(requestId));
    }

    public static String getRemoteAddr(HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");
        if (ip == null) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }

    @RequestMapping(
            value = {"/ws/1.0/request"},
            method = RequestMethod.POST,
            produces = "application/json",
            consumes = "application/json")
    @SuppressWarnings("ThrowableResultIgnored")
    public ResponseEntity<ReportResponse> request(
            @RequestBody(required = true) ReportRequest req,
            HttpServletRequest request
    ) {
        if (req.getReportParams().get("_USER_IP") == null) {
            req.getReportParams().put("_USER_IP", getRemoteAddr(request));
        }
        ReportTask res = reportService.request(req);
        HttpStatus status = res.getStatus().getHttpStatus();
        if (status == HttpStatus.PROCESSING)
            status = HttpStatus.ACCEPTED;
        return ResponseEntity.status(status).body(res.getResponse());
    }
}
