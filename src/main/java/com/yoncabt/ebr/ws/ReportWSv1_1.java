/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoncabt.ebr.ws;

import com.yoncabt.abys.core.util.log.FLogManager;
import com.yoncabt.ebr.ReportService;
import com.yoncabt.ebr.executor.ReportTask;
import com.yoncabt.ebr.executor.Status;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.mvc.method.annotation.AbstractJsonpResponseBodyAdvice;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author myururdurmaz
 */
@RestController
@Scope(scopeName = "request")
public class ReportWSv1_1 {

    @ControllerAdvice
    static class JsonpAdvice extends AbstractJsonpResponseBodyAdvice {
        public JsonpAdvice() {
            super("callback");
        }
    }

    @Autowired
    private ReportService reportService;

    private static final FLogManager logManager = FLogManager.getLogger(ReportTask.class);
    
    
    public static class StatusResponse implements Serializable {
        private Status status;

        public StatusResponse(Status status) {
            this.status = status;
        }
        

        /**
         * @return the status
         */
        public Status getStatus() {
            return status;
        }

        /**
         * @param status the status to set
         */
        public void setStatus(Status status) {
            this.status = status;
        }
    }


    @RequestMapping(
            value = {"/ws/1.1/status/{requestId}"},
            method = RequestMethod.GET,
            produces = "application/json")
    public ResponseEntity<StatusResponse> status(
            @PathVariable("requestId") String requestId
    ) {
        ReportTask detail = reportService.detail(requestId);
        if (detail == null) {//başlamamış
            logManager.info("status query :YOK !!! " + requestId);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        logManager.info("status query :" + requestId);
        switch (detail.getStatus()) {
            case WAIT:
                logManager.info(detail.getRequest().getReport() + " status query :" + requestId + " :başlamış");
                return ResponseEntity.status(HttpStatus.OK).body(new StatusResponse(detail.getStatus()));

            case RUN:
                logManager.info(detail.getRequest().getReport() + " status query :" + requestId + " :devam ediyor");
                return ResponseEntity.status(HttpStatus.OK).body(new StatusResponse(detail.getStatus()));

            case EXCEPTION:
                logManager.info(detail.getRequest().getReport() + " status query :" + requestId + " :hata");
                return ResponseEntity.status(HttpStatus.OK).body(new StatusResponse(detail.getStatus()));// 420 Method Failure

            case FINISH:
                logManager.info(detail.getRequest().getReport() + " status query :" + requestId + " :bitmiş");
                return ResponseEntity.status(HttpStatus.OK).body(new StatusResponse(detail.getStatus()));

            case CANCEL:
                logManager.info(detail.getRequest().getReport() + " status query :" + requestId + " :iptal");
                return ResponseEntity.status(HttpStatus.OK).body(new StatusResponse(detail.getStatus()));

            case SCHEDULED:
                logManager.info(detail.getRequest().getReport() + " status query :" + requestId + " :başlamış");
                return ResponseEntity.status(HttpStatus.OK).body(new StatusResponse(detail.getStatus()));
            default:
                throw new IllegalArgumentException(detail.getStatus().name());
        }
    }
}
