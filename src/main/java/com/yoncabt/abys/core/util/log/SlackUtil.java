/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoncabt.abys.core.util.log;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

/**
 *
 * @author myururdurmaz
 */
public class SlackUtil {
    /**
     *
     * @param icon
     * @param from ABYSConf.INSTANCE.getValue("abys.hipchat.from", "ABYS") kullanılabilir
     * @param message gönderilecek mesaj
     * @throws java.io.IOException hata oluşması durumunda
     */
    public static void sendMessage(String icon, String from, String message) throws IOException {

        Map<String, String> headers = new HashMap<>();
        headers.put("Content-type", "application/json");
        JSONObject jo = new JSONObject();
        jo.put("channel", "#general");
        jo.put("username", from);
        jo.put("text", message);
        jo.put("icon_emoji", icon);
        HttpUtil.request("https://hooks.slack.com/services/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", "POST", null, null, headers, jo.toString());
    }
//
//    public static void main(String[] args) throws IOException {
//        sendMessage(":card_file_box:", "MasterFS", "deneme mesajı");
//    }

}
