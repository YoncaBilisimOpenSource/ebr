/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoncabt.abys.core.util.log;

import com.yoncabt.ebr.exceptions.ReportException;
import com.yoncabt.ebr.util.VersionUtil;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

import org.apache.commons.lang.exception.ExceptionUtils;

/**
 * kodun son hali her zaman MasterFS içinde olsun
 * @author myururdurmaz
 */
public class ExceptionReporter extends Thread {

    private String msg;
    private Throwable throwable;

    public ExceptionReporter(String msg, Throwable throwable) {
        this.msg = msg;
        this.throwable = throwable;
    }

    @Override
    public void run() {
        String user = System.getenv("USER");
        String ip = getIp();
        String host = "";
        String name = "EBR " + VersionUtil.GIT_ID;
        try {
            host = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
        String message = "User: " + user +
                "\nIP: " + ip +
                "\nHOST: " + host +
                "\nMSG:" + msg;
        if (throwable instanceof ReportException) {
            ReportException re = (ReportException) throwable;
            if (re.getJrxml() != null) {
                message += "\nJRXML:" + re.getJrxml().getAbsolutePath();
            }
            if (re.getRequest() != null) {
                message += "\nREPORT:" + re.getRequest().getReport();
                message += "\nEXT:" + re.getRequest().getExtension();
                message += "\nUUID:" + re.getRequest().getUuid();
                message += "\nPAGE:" + re.getRequest().getReportParams().get("_USER_PAGE");
                message += "\nIP:" + re.getRequest().getReportParams().get("_USER_IP");
            }
        }
        if (throwable != null)
            message += "\n" + ExceptionUtils.getStackTrace(throwable);
        try {
            SlackUtil.sendMessage(":printer:", name + "-" + host + " - " + ip, message);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        try {
            TelegramUtil.sendMessage(":printer:", name + "-" + host + " - " + ip, message);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private String getIp() {
        StringBuilder sb = new StringBuilder();
        try {
            Enumeration e = NetworkInterface.getNetworkInterfaces();
            while(e.hasMoreElements()) {
                NetworkInterface n = (NetworkInterface) e.nextElement();
                Enumeration ee = n.getInetAddresses();
                while (ee.hasMoreElements()) {
                    InetAddress i = (InetAddress) ee.nextElement();
                    if (i.getHostAddress().equals("127.0.0.1"))
                        continue;
                    if (i.getHostAddress().contains(":"))
                        continue;
                    sb.append("[").append(i.getHostAddress()).append("]");
                }
            }
        } catch (SocketException ex) {
            ex.printStackTrace();
        }
        return sb.toString();
    }
//
//    public static void main(String[] args) {
//        new ExceptionReporter("deneme", new Exception()).start();
//    }
}
