package com.yoncabt.abys.core.util.log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.StopWatch;

import com.yoncabt.abys.core.util.EBRConf;

public class HttpUtil {

    private static final ThreadLocal<Boolean> fullCurlCommand = new ThreadLocal<Boolean>();
    private static final AtomicLong counter = new AtomicLong();

    private static InputStream clone(InputStream input) throws IOException {
        ByteArrayOutputStream bous = new ByteArrayOutputStream();
        IOUtils.copy(input, bous);
        return new ByteArrayInputStream(bous.toByteArray());
    }

    /**
     * burası sadece bu thread için curl logunu tamamen açar. ilk istekten sonra geri kapanır
     */
    public static void logFullCurlCommand() {
        fullCurlCommand.set(true);
    }

    private static final FLogManager logger = FLogManager.getLogger(HttpUtil.class);

    public static class HttpException extends IOException {

        private int statusCode;
        private InputStream stream = null;


        public HttpException(int statusCode, String message, Throwable cause) {
            super(message, cause);
            this.statusCode = statusCode;
        }

        public HttpException(int statusCode, String message, InputStream stream) {
            super(message);
            this.statusCode = statusCode;
            this.stream = stream;
        }

        public int getStatusCode() {
            return statusCode;
        }

        public InputStream getStream() {
            return stream;
        }

        @Override
        public String getMessage() {
            return getStatusCode() + " " + super.getMessage();
        }
    }

    public static InputStream request(String url, String method, String userName, String password,
            Map<String, String> headers,
            String body) throws IOException {
        return request(url, method, userName, password, headers, new HashMap<String, List<String>>(), null, body, 120000);
    }

    public static InputStream request(String url, String method, String userName, String password, Map<String, String> headers, String body, String cookie) throws IOException {
        return request(url, method, userName, password, headers, new HashMap<String, List<String>>(), cookie, body, 120000);
    }

    public static InputStream request(String url, String method, String userName, String password,
            Map<String, String> headers,
            Map<String, List<String>> responseHeaders,
            String cookie,
            String body, int timeoutMs) throws IOException {
        long cnt = counter.incrementAndGet();
        StopWatch watch = new StopWatch();
        watch.start();
        URL u = new URL(url);
        HttpURLConnection con = (HttpURLConnection) u.openConnection();
        // ornek kullanim: .setRequestProperty("Cookie", "key_1=value_1;
        // key_2=value_2; key_3=value_3");
        if (cookie != null) {
            con.setRequestProperty("Cookie", cookie);
        }
        con.setConnectTimeout(timeoutMs);
        con.setReadTimeout(timeoutMs);
        con.setRequestMethod(method);
        con.setDoOutput(body != null);
        if (StringUtils.isNotEmpty(userName)) {
            String userpass = userName + ":" + password;
            String basicAuth = "Basic " + new String(new Base64().encode(userpass.getBytes()));
            con.setRequestProperty("Authorization", basicAuth);
        }
        if (!MapUtils.isEmpty(headers)) {
            Iterator<String> it = headers.keySet().iterator();
            while (it.hasNext()) {
                String key = it.next();
                String value = headers.get(key);
                con.setRequestProperty(key, value);
            }
        }

        String curl = "curl ";

        if (StringUtils.isNotEmpty(body)) {
            curl += " --data '" + body.replace("'", "\\'") + "'";
        }
        if (headers != null) {
            for (Map.Entry<String, String> entry
                    : headers.entrySet()) {
                curl += " --header '" + entry.getKey() + ": " + entry.getValue() + "'";
            }
        }
        if (cookie != null)
            curl += " --header 'Cookie: " + cookie + "'";
        curl += " " + url;
        if ((fullCurlCommand.get() != null && fullCurlCommand.get()) || EBRConf.INSTANCE.getValue("httputil.full_curl_command", Boolean.FALSE))
            logger.info("curl " + cnt + ":" + curl);
        else
            logger.info("curl " + cnt + ":" + StringUtils.abbreviate(curl, 1000));
        fullCurlCommand.set(false);


        if (body != null) {
            try (OutputStream os = con.getOutputStream()) {
                IOUtils.write(body, os, "utf-8");
                os.flush();
            }
        }

        if (responseHeaders != null) {
            for (Map.Entry<String, List<String>> entry : con.getHeaderFields().entrySet()) {
                String key = entry.getKey();
                List<String> value = entry.getValue();
                responseHeaders.put(key, value);
            }
        }

        int responseCode = con.getResponseCode();
        InputStream is;
        if (responseCode < HttpURLConnection.HTTP_BAD_REQUEST) {
            is = con.getInputStream();
        } else {
            is = con.getErrorStream();
        }
        try {
        if (is == null) {
            throw new HttpException(responseCode, con.getResponseMessage(), is);
        }
        InputStream clone = clone(is);
        IOUtils.closeQuietly(is);

        if (responseCode != 200) {
            throw new HttpException(responseCode, con.getResponseMessage(), clone);
        }
        return clone;
        } finally {
            watch.stop();
            logger.info("curl " + cnt + ": running time " + watch);
            if (watch.getTime() > EBRConf.INSTANCE.getValue("httputil.full_curl_command_max_time", 1000L))
                logger.info("curl " + cnt + ":" + curl);
        }
    }
}
