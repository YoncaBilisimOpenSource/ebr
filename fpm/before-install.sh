#!/bin/sh

NAME=ebr
USER_NAME=eastblue

BASE=/var/$NAME
WAR=$BASE/$NAME.war
CONF=$BASE/$NAME.conf
SERVICE=$NAME

if ! grep $USER_NAME /etc/passwd > /dev/null
then
    useradd --system --create-home --home-dir $BASE --user-group $USER_NAME --shell /bin/bash
fi
