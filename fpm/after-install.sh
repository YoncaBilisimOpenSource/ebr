#!/bin/sh

NAME=ebr
USER_NAME=eastblue

BASE=/var/$NAME
WAR=$BASE/$NAME.war
CONF=$BASE/$NAME.conf

test -f $CONF || cp $CONF.orig $CONF

if [ ! -d $BASE/reports ];
then
	cp $BASE/logo.png.orig $BASE/logo.png
	cp --recursive $BASE/reports.orig $BASE/reports
fi

chmod +x $WAR
chown -R $USER_NAME:$USER_NAME $BASE/

ln -sf $WAR /etc/init.d/$NAME

update-rc.d $NAME defaults

# keystore tanımlı olmayanlar için bir ayarlama
if ! grep system.javax.net.ssl.trustStore $CONF > /dev/null
then
    echo "system.javax.net.ssl.trustStore=/var/ebr/keystore" >> $CONF
    echo "system.javax.net.ssl.trustStorePassword=yonca123" >> $CONF
fi
